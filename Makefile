#######################################################################
#	> File Name: Makefile
#	> Author: chunk
#	> Mail: chunkplus@gmail.com 
#######################################################################

EXEC =fffuse
MOUNTPOINT=/mnt/fuse
TH ?= 5

#FLAGS
CC ?= gcc
override CFLAGS += -std=gnu99 -O2 -g3 -Wall -fmessage-length=0 `pkg-config fuse --cflags `

#LIBS
LDDIR =
LIBS =`pkg-config fuse --cflags --libs`
#INCLUDE
IDIR =	-I.
_DEPS =
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))
INCDIR = $(IDIR)

#OBJECTS
OBJDIR= ./obj
SRCDIR= .:src
SRCS = $(foreach dir,$(subst :, ,$(SRCDIR)),$(wildcard $(dir)/*.c))
OBJ_PATH = $(addprefix $(OBJDIR)/,$(subst :, ,$(SRCDIR)))
OBJS = $(addprefix $(OBJDIR)/,$(subst .c,.o,$(SRCS)))

#START
.PHONY:all clean

all:$(EXEC)

run:all
	./$(EXEC) $(TH) /mnt/ssd /mnt/hdd $(MOUNTPOINT) -o allow_other,attr_timeout=0

stop:all
	fusermount -u $(MOUNTPOINT)

$(EXEC):$(OBJS)
	$(CC) $(CFLAGS) -o $@  $^  $(INCDIR) $(LDDIR) $(LIBS)
	
$(OBJDIR)/%.o:%.c
	mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) -c -o $@  $< $(INCDIR)		

clean:
	rm -f -r $(EXEC) $(OBJDIR)
