#!/usr/bin/env sh
opt="$1"

SSD="/mnt/ssd"
HDD="/mnt/hdd"
FUSE="/mnt/fuse"

# DEV_SSD="/dev/ram0"
# DEV_HDD="/dev/ram1"
DEV_SSD="/dev/ram0"
DEV_HDD="/dev/ram1"


if [ "$opt" =  "clean" ]; then
	echo "*** cleaning ..."
	fusermount -u $FUSE 2>&1
	umount $SSD 2>&1
	umount $HDD 2>&1
	rm -rf $FUSE $SSD $HDD 2>&1
	mkdir -p $FUSE $SSD $HDD 2>&1
fi

mkfs.ext2 $DEV_SSD 2>&1
mount  $DEV_SSD $SSD 2>&1
mkfs.ext2 $DEV_HDD 2>&1
mount  $DEV_HDD $HDD 2>&1

