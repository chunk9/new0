# FFFuse
A fuse-based toy fs.
### requirements
	fuse	2.9
	gcc		4.8
	 OR
	clang	3.5
	Linux	3.13 (?)


### how to 
- copy [fffinit.sh](fffinit.sh) to your /mnt and run:
	
		# ./fffinit.sh clean

- compile **FFFuse** and start it with:

		# ./fffstart.sh
  or you can specify the compiler with:

		# CC=clang ./fffstart.sh

- then it is startd! a log file will be created under you /tmp.
