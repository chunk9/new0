/*
 * util.h
 *
 *  Created on: Dec 9, 2015
 *      Author: chunk
 */

#ifndef UTIL_H_
#define UTIL_H_

#define FUSE_USE_VERSION 26
#define _XOPEN_SOURCE 700

#define HAVE_SYS_XATTR_H 0
//#define L_SYS_XATTR_H 0

typedef int bool;
#define true 1
#define false 0

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <time.h>

// user xattr
#define str(s)  #s
#define userstat(attr)        str(user.fff.attr)

typedef struct fff_state {
    FILE *logfile;
    int threshold;
    char *ssdmount;
    char *hddmount;
    char *fffmount;
} fff_state;

#define FFF_DATA ((struct fff_state *) fuse_get_context()->private_data)

extern int fff_error(char *func, char *op);
extern int fff_push(const char *path);
extern int fff_shadow(const char *path);
extern char * fff_realpath(char fpath[PATH_MAX], const char *path);
extern char * fff_realpath_x(char fpath[PATH_MAX], const char *path);
extern char * fff_hiddenpath(char fpath[PATH_MAX], const char *path);

extern int fff_init(struct fuse_conn_info *conn);
extern int fff_getattr(const char *path, struct stat *statbuf);
extern int fff_readlink(const char *path, char *link, size_t size);
extern int fff_mknod(const char *path, mode_t mode, dev_t dev);
extern int fff_mkdir(const char *path, mode_t mode);
extern int fff_unlink(const char *path);
extern int fff_rmdir(const char *path);
extern int fff_symlink(const char *path, const char *link);
extern int fff_rename(const char *path, const char *newpath);
extern int fff_link(const char *path, const char *newpath);
extern int fff_chmod(const char *path, mode_t mode);
extern int fff_chown(const char *path, uid_t uid, gid_t gid);
extern int fff_truncate(const char *path, off_t newsize);
extern int fff_utime(const char *path, struct utimbuf *ubuf);
extern int fff_open(const char *path, struct fuse_file_info *fi);
extern int fff_read(const char *path, char *buf, size_t size, off_t offset,
        struct fuse_file_info *fi);
extern int fff_write(const char *path, const char *buf, size_t size, off_t offset,
        struct fuse_file_info *fi);
extern int fff_statfs(const char *path, struct statvfs *statv);
extern int fff_flush(const char *path, struct fuse_file_info *fi);
extern int fff_release(const char *path, struct fuse_file_info *fi);
extern int fff_fsync(const char *path, int datasync, struct fuse_file_info *fi);
#ifdef HAVE_SYS_XATTR_H
extern int fff_setxattr(const char *path, const char *name, const void *value, size_t size,
        int flags);
extern int fff_getxattr(const char *path, const char *name, void *value, size_t size);
extern int fff_listxattr(const char *path, char *list, size_t size);
extern int fff_removexattr(const char *path, const char *name);
#endif
extern int fff_opendir(const char *path, struct fuse_file_info *fi);
extern int fff_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset,
        struct fuse_file_info *fi);
extern int fff_releasedir(const char *path, struct fuse_file_info *fi);
extern int fff_fsyncdir(const char *path, int datasync, struct fuse_file_info *fi);
extern int fff_access(const char *path, int mask);
extern int fff_create(const char *path, mode_t mode, struct fuse_file_info *fi);
extern int fff_ftruncate(const char *path, off_t offset, struct fuse_file_info *fi);
extern int fff_fgetattr(const char *path, struct stat *statbuf, struct fuse_file_info *fi);

extern struct fuse_operations fff_oper;
extern struct fff_state *fff_user_data;
extern FILE *logfile;
extern const char *dhidden;

#endif /* UTIL_H_ */
