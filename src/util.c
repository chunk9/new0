/*
 * util.c
 *
 *  Created on: Dec 9, 2015
 *      Author: chunk
 */

#include "util.h"

FILE *logfile = NULL;
const char *dhidden = "/.fff_hidden_343d9040a671c45832ee5381860e2996";  // md5("fff")
const char *nosuchfile = "/.fff_nosuch_343d9040a671c45832ee5381860e2996";
const char *xattrflag = "yes";
const bool xflag_set = true, xflag_unset = false;

inline char * fff_date() {
    char s[32];
    time_t t;
    time(&t);
    strftime(s, 32, "%T", localtime(&t));
    return s;
}

void log_msg(const char *format, ...) {
    va_list ap;
    va_start(ap, format);
    vfprintf(logfile, format, ap);
}

int fff_error(char *func, char *op) {
    log_msg("\t[ERROR] - [%s] - {%s: %s}\n", func, op, strerror(errno));
    return -errno;
}

char * fff_realpath(char fpath[PATH_MAX], const char *path) {
    strcpy(fpath, FFF_DATA->ssdmount);
    strncat(fpath, path, PATH_MAX);
    log_msg("\t[DEBUG] - [%s] - {path=\"%s\", fpath=\"%s\"}\n", __func__, path, fpath);
    return fpath;
}

char * fff_realpath_x(char fpath[PATH_MAX], const char *path) {
    strcpy(fpath, FFF_DATA->hddmount);
    strncat(fpath, path, PATH_MAX);
    log_msg("\t[DEBUG] - [%s] - {path=\"%s\", fpath=\"%s\"}\n", __func__, path, fpath);
    return fpath;
}

char * fff_hiddenpath(char fpath[PATH_MAX], const char *path) {
    strcpy(fpath, dhidden);
    strncat(fpath, path, PATH_MAX);
    log_msg("\t[DEBUG] - [%s] - {path=\"%s\", hpath=\"%s\"}\n", __func__, path, fpath);
    return fpath;
}

int fff_init(struct fuse_conn_info *conn) {
    int retstat = 0;
    char xattr[BUFSIZ] = { 0 };

    if ((retstat = fff_mkdir(dhidden, 0775)) == 0) {
#ifdef HAVE_SYS_XATTR_H
        if (fff_setxattr(dhidden, "user.fff.x", &xflag_set, sizeof(bool), 0) < 0) {
            fff_error(__func__, "FFFFFFFFFFFFFFFFFFFFFF");
            exit(EXIT_FAILURE);
        }
#endif
    }
    log_msg("\t[DEBUG] - [%s] - {ret=%d}\n", __func__, fff_listxattr(dhidden, xattr, BUFSIZ));
    return fff_user_data;
}

int cp(const char *to, const char *from) {
    int fd_to, fd_from;
    char buf[4096];
    ssize_t nread;
    int saved_errno;
    struct stat st;

    if (stat(from, &st) != 0)
        return -1;
    fd_from = open(from, O_RDONLY);
    if (fd_from < 0)
        return -1;
    fd_to = open(to, O_WRONLY | O_CREAT | O_APPEND, st.st_mode);
    if (fd_to < 0)
        goto out_error;

    /**
     * mode-setting on creating failed because of global umask issues.
     *  Ref - http://stackoverflow.com/questions/12391265/file-permission-problems-in-unix
     */
    chmod(to, st.st_mode);

    while (nread = read(fd_from, buf, sizeof buf), nread > 0) {
        char *out_ptr = buf;
        ssize_t nwritten;
        do {
            nwritten = write(fd_to, out_ptr, nread);
            if (nwritten >= 0) {
                nread -= nwritten;
                out_ptr += nwritten;
            } else if (errno != EINTR) {
                goto out_error;
            }
        } while (nread > 0);
    }
    if (nread == 0) {
        if (close(fd_to) < 0) {
            fd_to = -1;
            goto out_error;
        }
        close(fd_from);
        /* Success! */
        return 0;
    }
    out_error: saved_errno = errno;
    close(fd_from);
    if (fd_to >= 0)
        close(fd_to);

    errno = saved_errno;
    return -1;
}

int shadow(const char *to, const char *from) {
    int fd_to;
    struct stat st;

    if (stat(from, &st) != 0)
        return -1;
    fd_to = open(to, O_WRONLY | O_CREAT | O_APPEND, st.st_mode);
    if (fd_to >= 0) {
        chmod(to, st.st_mode);
        close(fd_to);
        return 0;
    }
    return -1;
}

int fff_push(const char *path) {
    char fpath_from[PATH_MAX], fpath_to[PATH_MAX];

    fff_realpath(fpath_from, path);
    fff_realpath_x(fpath_to, path);

    log_msg("\t[DEBUG] - [%s] - {FFFFFFFFFFFFFFFFFFFFFF COPY from=\"%s\", to=\"%s\"}\n", __func__,
            fpath_from, fpath_to);
    return cp(fpath_to, fpath_from);
}

int fff_shadow(const char *path) {
    char fpath[PATH_MAX], fpath_h[PATH_MAX], hpath[PATH_MAX];
    int ret = 0;
    struct stat st;

    fff_realpath(fpath, path);
    fff_hiddenpath(hpath, path);
    fff_realpath(fpath_h, hpath);

    log_msg("\t[DEBUG] - [%s] - {FFFFFFFFFFFFFFFFFFFFFF SHADOW from=\"%s\", to=\"%s\"}\n", __func__,
            fpath, fpath_h);

    if ((ret = shadow(fpath_h, fpath)) == 0) {
#ifdef HAVE_SYS_XATTR_H
        if (stat(fpath, &st) != 0)
            return -1;
        if (fff_setxattr(hpath, "user.fff.x", &xflag_set, sizeof(bool), 0) < 0) {
            fff_error(__func__, "FFFFFFFFFFFFFFFFFFFFFF");
            exit(EXIT_FAILURE);
        }
        if (fff_setxattr(hpath, "user.fff.stat", &st, sizeof(st), 0) < 0) {
            fff_error(__func__, "FFFFFFFFFFFFFFFFFFFFFF");
            exit(EXIT_FAILURE);
        }
#endif
        return 0;
    }
    return ret;
}

int fff_getattr(const char *path, struct stat *statbuf) {
    int retstat = 0;
    char fpath[PATH_MAX], fpath_x[PATH_MAX], hpath[PATH_MAX];
    int a = 0;
    struct stat st;

//    log_msg("\n[%s] - [%s] - {path=\"%s\", statbuf=0x%08x}\n", fff_date(), __func__, path,
//            statbuf);
    log_msg("\n[%s] - [%s] - {path=\"%s\"}\n", fff_date(), __func__, path);
    fff_realpath(fpath, path);
    fff_hiddenpath(hpath, path);
    fff_realpath(fpath_x, hpath);

    retstat = lstat(fpath, statbuf);
    if (retstat != 0)
        retstat = fff_error(__func__, "lstat");
#ifdef HAVE_SYS_XATTR_H
    if (fff_getxattr(hpath, "user.fff.x", &a, sizeof(int)) == sizeof(int) && a == xflag_set)
        if (fff_getxattr(hpath, "user.fff.stat", &st, sizeof(st)) == sizeof(st)) {
            log_msg("\t[WARN*] - [%s] - {FFFFFFFFFFFFFFFFFFFFFF ATTR %lu}\n", __func__, st.st_ino);
            *statbuf = st;

            if (retstat != 0)
                retstat = fff_error(__func__, "stat");
        }
#endif
    return retstat;
}

int fff_readlink(const char *path, char *link, size_t size) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", link=\"%s\", size=%d}\n", fff_date(), __func__, path,
            link, size);
    fff_realpath(fpath, path);

    retstat = readlink(fpath, link, size - 1);
    if (retstat < 0)
        retstat = fff_error(__func__, "readlink");
    else {
        link[retstat] = '\0';
        retstat = 0;
    }
    return retstat;
}

int fff_mknod(const char *path, mode_t mode, dev_t dev) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", mode=0%3o, dev=%lld}\n", fff_date(), __func__, path,
            mode, dev);
    fff_realpath(fpath, path);

// On Linux this could just be 'mknod(path, mode, rdev)' but this
//  is more portable
    if (S_ISREG(mode)) {
        retstat = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
        if (retstat < 0)
            retstat = fff_error(__func__, "open");
        else {
            retstat = close(retstat);
            if (retstat < 0)
                retstat = fff_error(__func__, "close");
        }
    } else if (S_ISFIFO(mode)) {
        retstat = mkfifo(fpath, mode);
        if (retstat < 0)
            retstat = fff_error(__func__, "mkfifo");
    } else {
        retstat = mknod(fpath, mode, dev);
        if (retstat < 0)
            retstat = fff_error(__func__, "mknod");
    }

    return retstat;
}

int fff_mkdir(const char *path, mode_t mode) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", mode=0%3o}\n", fff_date(), __func__, path, mode);
    fff_realpath(fpath, path);

    retstat = mkdir(fpath, mode);
    if (retstat < 0)
        retstat = fff_error(__func__, "mkdir");

    return retstat;
}

int fff_unlink(const char *path) {
    int retstat = 0;
    char fpath[PATH_MAX], fpath_x[PATH_MAX], hpath[PATH_MAX], fpath_h[PATH_MAX];
    int a = 0;

//    log_msg("\n[%s] - [%s] - {path=\"%s\", statbuf=0x%08x}\n", fff_date(), __func__, path,
//            statbuf);
    log_msg("\n[%s] - [%s] - {path=\"%s\"}\n", fff_date(), __func__, path);
    fff_realpath(fpath, path);
    fff_realpath_x(fpath_x, path);
    fff_hiddenpath(hpath, path);
    fff_realpath(fpath_h, hpath);

#ifdef HAVE_SYS_XATTR_H
    if (fff_getxattr(hpath, "user.fff.x", &a, sizeof(int)) == sizeof(int) && a == xflag_set) {
        log_msg("\t[WARN*] - [%s] - {FFFFFFFFFFFFFFFFFFFFFF ATTR %d}\n", __func__, a);

        retstat = unlink(fpath_h);
        if (retstat < 0)
            retstat = fff_error(__func__, "unlink");
        retstat = unlink(fpath_x);
        if (retstat < 0)
            retstat = fff_error(__func__, "unlink");
    }
#endif

    retstat = unlink(fpath);
    if (retstat < 0)
        retstat = fff_error(__func__, "unlink");

    return retstat;
}

int fff_rmdir(const char *path) {
    int retstat = 0;
    char fpath[PATH_MAX], fpath_x[PATH_MAX], hpath[PATH_MAX], fpath_h[PATH_MAX];
    int a = 0;

    log_msg("\n[%s] - [%s] - {path=\"%s\"}\n", fff_date(), __func__, path);
    fff_realpath(fpath, path);
    fff_realpath_x(fpath_x, path);
    fff_hiddenpath(hpath, path);
    fff_realpath(fpath_h, hpath);

#ifdef HAVE_SYS_XATTR_H

    retstat = rmdir(fpath_h);
    if (retstat < 0)
        retstat = fff_error(__func__, "rmdir");
    retstat = rmdir(fpath_x);
    if (retstat < 0)
        retstat = fff_error(__func__, "rmdir");

#endif

    retstat = rmdir(fpath);
    if (retstat < 0)
        retstat = fff_error(__func__, "rmdir");

    return retstat;
}

int fff_symlink(const char *path, const char *link) {
    int retstat = 0;
    char flink[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", link=\"%s\"}\n", fff_date(), __func__, path, link);
    fff_realpath(flink, link);

    retstat = symlink(path, flink);
    if (retstat < 0)
        retstat = fff_error(__func__, "symlink");

    return retstat;
}

int fff_rename(const char *path, const char *newpath) {
    int retstat = 0;
    char fpath[PATH_MAX];
    char fnewpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {fpath=\"%s\", newpath=\"%s\"}\n", fff_date(), __func__, path,
            newpath);
    fff_realpath(fpath, path);
    fff_realpath(fnewpath, newpath);

    retstat = rename(fpath, fnewpath);
    if (retstat < 0)
        retstat = fff_error(__func__, "rename");

    return retstat;
}

int fff_link(const char *path, const char *newpath) {
    int retstat = 0;
    char fpath[PATH_MAX], fnewpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", newpath=\"%s\"}\n", fff_date(), __func__, path, newpath);
    fff_realpath(fpath, path);
    fff_realpath(fnewpath, newpath);

    retstat = link(fpath, fnewpath);
    if (retstat < 0)
        retstat = fff_error(__func__, "link");

    return retstat;
}

int fff_chmod(const char *path, mode_t mode) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {fpath=\"%s\", mode=0%03o}\n", fff_date(), __func__, path, mode);
    fff_realpath(fpath, path);

    retstat = chmod(fpath, mode);
    if (retstat < 0)
        retstat = fff_error(__func__, "chmod");

    return retstat;
}

int fff_chown(const char *path, uid_t uid, gid_t gid) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", uid=%d, gid=%d}\n", fff_date(), __func__, path, uid,
            gid);
    fff_realpath(fpath, path);

    retstat = chown(fpath, uid, gid);
    if (retstat < 0)
        retstat = fff_error(__func__, "chown");

    return retstat;
}

int fff_truncate(const char *path, off_t newsize) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", newsize=%lld}\n", fff_date(), __func__, path, newsize);
    fff_realpath(fpath, path);

    retstat = truncate(fpath, newsize);
    if (retstat < 0)
        fff_error(__func__, "truncate");

    return retstat;
}

int fff_utime(const char *path, struct utimbuf *ubuf) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", ubuf=0x%08x}\n", fff_date(), __func__, path, ubuf);
    fff_realpath(fpath, path);

    retstat = utime(fpath, ubuf);
    if (retstat < 0)
        retstat = fff_error(__func__, "utime");

    return retstat;
}

int fff_open(const char *path, struct fuse_file_info *fi) {
    int retstat = 0;
    int fd;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path\"%s\", fi=0x%08x}\n", fff_date(), __func__, path, fi);
    fff_realpath(fpath, path);

    fd = open(fpath, fi->flags);
    if (fd < 0)
        retstat = fff_error(__func__, "open");

    fi->fh = fd;
//	close(fd);

    return retstat;
}

int fff_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    int retstat = 0;
    int fd;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {(path=\"%s\", buf=0x%08x, size=%d, offset=%lld, fi=0x%08x}\n",
            fff_date(), __func__, path, buf, size, offset, fi);

    retstat = pread(fi->fh, buf, size, offset);
    if (retstat < 0)
        retstat = fff_error(__func__, "read");

    return retstat;
}

int fff_write(const char *path, const char *buf, size_t size, off_t offset,
        struct fuse_file_info *fi) {
    int retstat = 0;
    int fd;

    struct stat st, st2;

    log_msg("\n[%s] - [%s] - {path=\"%s\", buf=0x%08x, size=%d, offset=%lld, fi=0x%08x}\n",
            fff_date(), __func__, path, buf, size, offset, fi);

    retstat = pwrite(fi->fh, buf, size, offset);
    if (retstat < 0)
        retstat = fff_error(__func__, "pwrite");

    return retstat;
}

int fff_statfs(const char *path, struct statvfs *statv) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", statv=0x%08x}\n", fff_date(), __func__, path, statv);
    fff_realpath(fpath, path);

    retstat = statvfs(fpath, statv);
    if (retstat < 0)
        retstat = fff_error(__func__, "statvfs");
    return retstat;
}

int fff_flush(const char *path, struct fuse_file_info *fi) {
    /* Just a stub.	 This method is optional and can safely be left
     unimplemented */
    int retstat = 0;

    log_msg("\n[%s] - [%s] - {path=\"%s\", fi=0x%08x}\n", fff_date(), __func__, path, fi);
    return retstat;
}

int fff_release(const char *path, struct fuse_file_info *fi) {
    int retstat = 0;
    char fpath[PATH_MAX], fpath_x[PATH_MAX], hpath[PATH_MAX], fpath_h[PATH_MAX];
    char fpathtmp[PATH_MAX], fpathtmp_x[PATH_MAX];
    struct stat st;

    log_msg("\n[%s] - [%s] - {path=\"%s\", fi=0x%08x}\n", fff_date(), __func__, path, fi);
    fff_realpath(fpath, path);
    fff_realpath_x(fpath_x, path);
    fff_hiddenpath(hpath, path);
    fff_realpath(fpath_h, hpath);

    log_msg("\t[WARN*] - [%s] - {FFFFFFFFFFFFFFFFFFFFFF REAL %s,%s}\n", __func__,
            realpath(fpath, fpathtmp), realpath(fpath_x, fpathtmp_x));

    if (fpathtmp_x != NULL && !strcmp(fpathtmp, fpathtmp_x)) {
#ifdef HAVE_SYS_XATTR_H
        if (stat(fpath_x, &st) != 0)
            return -1;
        if (fff_setxattr(hpath, "user.fff.stat", &st, sizeof(st), 0) < 0) {
            fff_error(__func__, "FFFFFFFFFFFFFFFFFFFFFF");
            exit(EXIT_FAILURE);
        }
#endif
    } else if (stat(fpath, &st) == 0 && st.st_size > FFF_DATA->threshold) {
        log_msg("\t[WARN*] - [%s] - {FFFFFFFFFFFFFFFFFFFFFF SIZE (%d)}\n", __func__, st.st_size);
        if (rename(fpath, fpath_x))
            if (errno == EXDEV) {
                fff_push(path);
                fff_unlink(path);
                fff_symlink(fpath_x, path);
#ifdef HAVE_SYS_XATTR_H
                fff_shadow(path);
#endif
            } else if (errno == ENOENT) {
                char * tmppath = strdup(fpath), *tmppath2 = strdup(fpath_x), *tmppath3 = strdup(
                        fpath_h);
                char * tmpdir = dirname(tmppath), *tmpdir2 = dirname(tmppath2), *tmpdir3 = dirname(
                        tmppath3);
                if (stat(tmpdir, &st) != -1 && mkdir(tmpdir2, st.st_mode) == 0
                        && mkdir(tmpdir3, st.st_mode) == 0) {
                    fff_push(path);
                    fff_unlink(path);
                    fff_symlink(fpath_x, path);
#ifdef HAVE_SYS_XATTR_H
                    fff_shadow(path);
#endif
                }
            } else {
                fff_error(__func__, "FFFFFFFFFFFFFFFFFFFFFF");
                perror("rename");
                exit(EXIT_FAILURE);
            }
        log_msg("\t[WARN*] - [%s] - {FFFFFFFFFFFFFFFFFFFFFF LINK %s,%s}\n", __func__,
                realpath(fpath, fpathtmp), realpath(fpath_x, fpathtmp_x));
    }
    return retstat;
}

int fff_fsync(const char *path, int datasync, struct fuse_file_info *fi) {
    int retstat = 0;

    log_msg("\n[%s] - [%s] - {path=\"%s\", datasync=%d, fi=0x%08x}\n", fff_date(), __func__, path,
            datasync, fi);
    retstat = fsync(fi->fh);

    if (retstat < 0)
        fff_error(__func__, "fsync");

    return retstat;
}
#ifdef HAVE_SYS_XATTR_H
int fff_setxattr(const char *path, const char *name, const void *value, size_t size, int flags) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", name=\"%s\", value=0x%08x, size=%d, flags=0x%08x}\n",
            fff_date(), __func__, path, name, value, size, flags);
    fff_realpath(fpath, path);

//    retstat=lsetxattr(fpath, name, value, size, flags);
    retstat = setxattr(fpath, name, value, size, flags);
    if (retstat < 0)
        retstat = fff_error(__func__, "setxattr");

    return retstat;
}

int fff_getxattr(const char *path, const char *name, void *value, size_t size) {
    int retstat = 0;
    char fpath[PATH_MAX];

//    log_msg("\n[%s] - [%s] - {path=\"%s\", name=\"%s\", value=0x%08x, size=%d}\n",
//            fff_date(), __func__, path, name, value, size);
    log_msg("\n[%s] - [%s] - {path=\"%s\", name=\"%s\", size=%d}\n", fff_date(), __func__, path,
            name, size);
    fff_realpath(fpath, path);

//    retstat=lgetxattr(fpath, name, value, size);
    retstat = getxattr(fpath, name, value, size);
//    log_msg("\t[DEBUG] - [%s] - {FFFFFFFFFFFFFFFFFFFFFF GETATTR %d}\n", __func__, retstat);
    if (retstat < 0)
        retstat = fff_error(__func__, "getxattr");
    else
        log_msg("\t[DEBUG] - [%s] - {retstat=%d, *value=0x%08x, sr(value)=\"%s\"}\n", __func__,
                retstat, *((const int *) value), value);
    return retstat;
}

int fff_listxattr(const char *path, char *list, size_t size) {
    int retstat = 0;
    char fpath[PATH_MAX];
    char *ptr;

    log_msg("\n[%s] - [%s] - {path=\"%s\", list=0x%08x, size=%d}\n", fff_date(), __func__, path,
            list, size);
    fff_realpath(fpath, path);

//    retstat=llistxattr(fpath, list, size);
    retstat = listxattr(fpath, list, size);
    if (retstat < 0)
        retstat = fff_error(__func__, "listxattr");

    log_msg("\t[DEBUG] - [%s] - {returned attributes (length %d)}\n", __func__, retstat);
    for (ptr = list; ptr < list + retstat; ptr += strlen(ptr) + 1)
        log_msg("\t    \"%s\"\n", ptr);

    return retstat;
}

int fff_removexattr(const char *path, const char *name) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", name=\"%s\"}\n", fff_date(), __func__, path, name);
    fff_realpath(fpath, path);

//    retstat=lremovexattr(fpath, name);
    retstat = removexattr(fpath, name);
    if (retstat < 0)
        retstat = fff_error(__func__, "rmovexattr");

    return retstat;
}
#endif

int fff_opendir(const char *path, struct fuse_file_info *fi) {
    DIR *dp;
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", fi=0x%08x}\n", fff_date(), __func__, path, fi);
    fff_realpath(fpath, path);

    dp = opendir(fpath);
    if (dp == NULL)
        retstat = fff_error(__func__, "opendir");
    else {
        fi->fh = (intptr_t) dp;
        log_msg("\t[DEBUG] - [%s] - {fh=0x%016x}\n", __func__, fi->fh);
    }

    return retstat;
}

int fff_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset,
        struct fuse_file_info *fi) {
    int retstat = 0;
    DIR *dp;
    struct dirent *de;

    log_msg("\n[%s] - [%s] - {path=\"%s\", offset=%lld, fi=0x%08x, fh=0x%016x}\n", fff_date(),
            __func__, path, offset, fi, fi->fh);
    dp = (DIR *) (uintptr_t) fi->fh;
    de = readdir(dp);
    if (de == 0) {
        retstat = fff_error(__func__, "readdir");
        return retstat;
    }
    do {
        /**
         * a FAKE file mask!
         * we hide the dir of attributes container while allowing file access through full path.
         */
        if (!strcmp(de->d_name, &dhidden[1]))
            continue;
        if (filler(buf, de->d_name, NULL, 0) != 0) {
            fff_error(__func__, "filler:  buffer full");
            return -ENOMEM;
        }
        log_msg("\t[DEBUG] - [%s] - {d_name=\"%s\"}\n", __func__, de->d_name);
    } while ((de = readdir(dp)) != NULL);
    return retstat;
}

int fff_releasedir(const char *path, struct fuse_file_info *fi) {
    int retstat = 0;

    log_msg("\n[%s] - [%s] - {path=\"%s\", fi=0x%08x}\n", fff_date(), __func__, path, fi);
    closedir((DIR *) (uintptr_t) fi->fh);

    return retstat;
}

int fff_fsyncdir(const char *path, int datasync, struct fuse_file_info *fi) {
    int retstat = 0;

    log_msg("\n[%s] - [%s] - {path=\"%s\", datasync=%d, fi=0x%08x}\n", fff_date(), __func__, path,
            datasync, fi);
    return retstat;
}

int fff_access(const char *path, int mask) {
    int retstat = 0;
    char fpath[PATH_MAX];

    log_msg("\n[%s] - [%s] - {path=\"%s\", mask=0%o}\n", fff_date(), __func__, path, path, mask);
    fff_realpath(fpath, path);

    retstat = access(fpath, mask);

    if (retstat < 0)
        retstat = fff_error(__func__, "access");

    return retstat;
}

int fff_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    int retstat = 0;
    char fpath[PATH_MAX];
    int fd;

    log_msg("\n[%s] - [%s] - {path=\"%s\", mode=0%03o, fi=0x%08x}\n", fff_date(), __func__, path,
            mode, fi);
    fff_realpath(fpath, path);

    fd = creat(fpath, mode);
    if (fd < 0)
        retstat = fff_error(__func__, "creat");

    fi->fh = fd;

    return retstat;
}

/**
 * Change the size of an open file
 *
 * This method is called instead of the truncate() method if the
 * truncation was invoked from an ftruncate() system call.
 *
 * If this method is not implemented or under Linux kernel
 * versions earlier than 2.6.15, the truncate() method will be
 * called instead.
 *
 * Introduced in version 2.5
 */
int fff_ftruncate(const char *path, off_t offset, struct fuse_file_info *fi) {
    int retstat = 0;

    log_msg("\n[%s] - [%s] - {path=\"%s\", offset=%lld, fi=0x%08x}\n", fff_date(), __func__, path,
            offset, fi);
    retstat = ftruncate(fi->fh, offset);
    if (retstat < 0)
        retstat = fff_error(__func__, "ftruncate");

    return retstat;
}

/**
 * Get attributes from an open file
 *
 * This method is called instead of the getattr() method if the
 * file information is available.
 *
 * Currently this is only called after the create() method if that
 * is implemented (see above).  Later it may be called for
 * invocations of fstat() too.
 *
 * Introduced in version 2.5
 */
int fff_fgetattr(const char *path, struct stat *statbuf, struct fuse_file_info *fi) {
    int retstat = 0;

    log_msg("\n[%s] - [%s] - {path=\"%s\", statbuf=0x%08x, fi=0x%08x}\n", fff_date(), __func__,
            path, statbuf, fi);
// On FreeBSD, trying to do anything with the mountpoint ends up
// opening it, and then using the FD for an fgetattr.  So in the
// special case of a path of "/", I need to do a getattr on the
// underlying root directory instead of doing the fgetattr().
    if (!strcmp(path, "/"))
        return fff_getattr(path, statbuf);

    retstat = fstat(fi->fh, statbuf);
    if (retstat < 0)
        retstat = fff_error(__func__, "fstat");

    return retstat;
}

struct fuse_operations fff_oper = { .init = fff_init, .getattr = fff_getattr, .readlink =
        fff_readlink, .getdir =
NULL, .mknod = fff_mknod, .mkdir = fff_mkdir, .unlink = fff_unlink, .rmdir = fff_rmdir, .symlink =
        fff_symlink, .rename = fff_rename, .link = fff_link, .chmod = fff_chmod, .chown = fff_chown,
        .truncate = fff_truncate, .utime = fff_utime, .open = fff_open, .read = fff_read, .write =
                fff_write, .statfs = fff_statfs, .flush = fff_flush, .release = fff_release,
        .fsync = fff_fsync,
#ifdef HAVE_SYS_XATTR_H
        .setxattr = fff_setxattr, .getxattr = fff_getxattr, .listxattr = fff_listxattr,
        .removexattr = fff_removexattr,
#endif
        .opendir = fff_opendir, .readdir = fff_readdir, .releasedir = fff_releasedir, .fsyncdir =
                fff_fsyncdir, .access = fff_access, .create = fff_create,
        .ftruncate = fff_ftruncate, .fgetattr = fff_fgetattr };

