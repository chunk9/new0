/*
 ============================================================================
 Name        : fffuse.c
 Author      : chunk
 Version     :
 Copyright   : Copyright (C) 2011-2016 Chunk <chunkplus@gmail.com>
 Description : Hello World!
 ============================================================================
 */

#include "util.h"

#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/xattr.h>

void Logo() {
    printf("\n");
    printf("\n");
    printf("\t\t================== !FFFuse! ================== \n");
    printf("\t\t            A simple fuse filesystem \n");
    printf("\t\t         by Chunk <chunkplus@gmail.com>\n\n\n");
}

void Usage() {
}

struct fff_state *fff_user_data;

int main(int argc, char *argv[]) {
    Logo();
    int fuse_stat;

//    if ((getuid() == 0) || (geteuid() == 0)) {
//        fprintf(stderr, "please run as non-root user!\n");
//        return 1;
//    }

    if (argc < 5)
        Usage();

    logfile = fopen("/tmp/fff.log", "w");
    if (logfile == NULL) {
        perror("logfile");
        exit(EXIT_FAILURE);
    }
    setvbuf(logfile, NULL, _IOLBF, 0);

    fff_user_data = malloc(sizeof(struct fff_state));
    if (fff_user_data == NULL) {
        perror("main calloc");
        abort();
    }

    if (argc == 2 && argv[argc - 1][0] == '-') {
    } else if (argc == 5) {
        fff_user_data->threshold = atoi(argv[1]);
        fff_user_data->ssdmount = realpath(argv[2], NULL);
        fff_user_data->hddmount = realpath(argv[3], NULL);
        fff_user_data->fffmount = realpath(argv[4], NULL);
        argv[1] = argv[4];
        argv[2] = NULL;
        argc = 2;
    } else if (argc >= 7 && argv[argc - 2][0] == '-') {
        fff_user_data->threshold = atoi(argv[1]);
        fff_user_data->ssdmount = realpath(argv[2], NULL);
        fff_user_data->hddmount = realpath(argv[3], NULL);
        fff_user_data->fffmount = realpath(argv[4], NULL);
        argc -= 3;
        for (int i = 1; i < argc; i++)
            argv[i] = argv[i + 3];
        argv[argc] = NULL;
    } else {
        Usage();
        return -1;
    }

    printf("starting fff ...\n");
    fuse_stat = fuse_main(argc, argv, &fff_oper, fff_user_data);

    return fuse_stat;
}
