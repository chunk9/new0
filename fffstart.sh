#!/usr/bin/env sh
make stop 2>&1
make clean 2>&1

th=5
if [ ! -z "$1" ]; then
  th=$1
fi
echo $th

TH=$th make CFLAGS=-DHAVE_SYS_XATTR_H=0  run 2>&1
